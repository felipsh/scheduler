from django.shortcuts import render, redirect
from booking.models import Booking
from booking.forms import BookingForm
from django.contrib.auth.decorators import login_required
from django.views.generic import FormView, TemplateView
from .forms import ContactForm
from django.urls import reverse_lazy

@login_required
def list_bookings(request):
    booking = Booking.objects.filter(patient=request.user)
    context = {
        "booking": booking,
    }
    return render(request, "booking/appointments.html", context)

@login_required
def show_booking(request, id):
    booking = Booking.objects.filter(id=id)
    context = {
        "booking": booking,
    }
    return render(request, "booking/index.html", context)

@login_required
def request_booking(request):
    if request.method == "POST":
        form = BookingForm(request.POST)
        if form.is_valid():
            booking = form.save()
            booking.patient = request.user
            booking.save()
            return redirect("list_bookings")
    else:
        form = BookingForm()
    context = {
        "form": form,
    }
    return render(request, "booking/request.html", context)

class ContactView(FormView):
    template_name = 'booking/contact.html'
    form_class = ContactForm
    success_url = reverse_lazy('success')

    def form_valid(self, form):
        form.send()
        return super().form_valid(form)
        # if request.method == "POST":
        #     form = ContactForm(request.POST)
        #     if form.is_valid():
        #         form.send()
        #         return redirect('contact:success') # may need to configure this url
        # else:
        #     form = ContactForm()
class ContactSuccessView(TemplateView):
    template_name = 'booking/success.html'
# Create your views here.
class HomeView(TemplateView):
    template_name = 'booking/home.html'
