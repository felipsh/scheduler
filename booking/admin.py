from django.contrib import admin
from booking.models import Booking

@admin.register(Booking)
class BookingAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "type",
        "date",
        "practitioner",
        "patient",
        "id",
    )
# Register your models here.
