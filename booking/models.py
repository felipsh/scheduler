from django.db import models
from django.contrib.auth.models import User

class Booking(models.Model):
    name = models.CharField(max_length = 200)
    type = models.CharField(max_length = 200)
    date = models.DateTimeField()
    practitioner = models.CharField(max_length = 200, null=True)
    patient = models.ForeignKey(
        User,
        related_name="booking",
        on_delete=models.CASCADE,
        null=True,
    )
# Create your models here.
