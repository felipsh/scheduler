from django.urls import path
from booking.views import list_bookings, show_booking, request_booking, ContactView, ContactSuccessView, HomeView

urlpatterns = [
    path("", list_bookings, name="list_bookings"),
    path("<int:id>/", show_booking, name="show_booking"),
    path("request/", request_booking, name="request_booking"),
    path("success/", ContactSuccessView.as_view(), name="success"),
    path("contact/", ContactView.as_view(), name="contact"),
    path("home/", HomeView.as_view(), name="home"),
]
